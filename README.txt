Etsy Treasury
==============================

Add an Etsy Treasury to your blog post quickly and easily with beautiful and customizable layouts. 

Treasuries are a powerful and expressive way to showcase Etsy items. This plugin adds an Etsy Treasury to a blog post with links to the items, the seller’s stores and the Treasury curator. The user can select the image sizes, columns and display layout to best fit their theme and taste.

If you are an Etsy seller, Treasuries are a terrific way to:

    Show off your items featured in another’s Treasury
    Add related content to a blog post
    Support members of an Etsy team by showing their items
    Display your taste and style in a personal way
    Build a mini store of your listings on your blog


Installation.
=============

1. You need to install shortcode (http://drupal.org/project/shortcode) module before setting up Etsy treasury module.

2. Unzip the files to the "sites/all/modules" directory and enable the module.

3. Go to the admin/config/content/formats page and click on the configure link of the format you need. 

4. On the configuration page, there is a 'Filter Settings' section. Click on the 'Shortcodes' link available there and click the checkbox against 'Enable Etsy Treasury shortcode' and save. 

5. Insert the etsy treasury shortcode in the page/blog post body field. Format of the etsy shortcode is as follows.

[etsy treasury="NzU1MzMxOXwxOTEwNTA3MDIz" size="large" display="image_only" columns=6] [/etsy] 

6. The 'treasury' attribute is used for specifying the etsy treasury id. The 'size' attribute is used for specifying the image size and it can have any one of the following - small, medium, large and very_large.
The 'display' attribute can be any one of the following - complete, image_only and tiled. The 'complete' attribute will show the listing name, listing link and shop name. The image_only' parameter will show only the images of the listings and their links and the 'tiled' parameter will list images as tiled. You can specify the table column count using 'columns' attribute.


For developers.
===============

If you want to add new size or display, you can easily extend by writing specialized classes for Display and View_Strategy classes available in the /lib directory of this module.
