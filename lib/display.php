<?php
abstract class Display {

	private $font_size;

	private $width;

	private $height;

	private $tiled_height;

	private $listing_title_length;

	private $shop_title_length;

	private $no_columns;

	private $view_strategy;

	private $treasury;

	public function __construct(View_Strategy $view_strategy, Treasury $treasury, $column_count) {
		$this->view_strategy = $view_strategy;
		$this->treasury = $treasury;
		$this->no_columns = $column_count;
	}

	public function generate() {
		return $this->view_strategy->generate($this);
	}

	abstract function set_properties();

	public function get_treasury() {
		return $this->treasury;
	}

	public function get_no_columns() {
		return $this->no_columns;
	}

}