<?php
abstract class View_Strategy {
	/**
	 * Generate treasury listing
	 *
	 * @param String $display_format Display format required
	 * @return String
	 */
	public function generate( $display ) {
		$obj_treasury = $display->get_treasury();
		$arr_listing_list = $obj_treasury->get_listing_list();
		if( !empty($arr_listing_list) ) {
			$no_of_items = sizeof($arr_listing_list);
			$listings = new ArrayIterator($arr_listing_list);
			$txt_html = $this->get_header_tag_html($obj_treasury);
			$txt_html .= $this->get_table_header_html();
			$txt_html .= $this->get_table_rows_html($obj_treasury, $listings, $no_of_items, $display);
			// Adding table closure
			$txt_html .= '</tbody></table>';
					
			return $txt_html;
		}
	}

	/**
	 * Get H2 tag html
	 *
	 * @param Treasury $obj_treasury Treasury object
	 * @return String
	 */
	private function get_header_tag_html( $obj_treasury ) {
		return '<h2 style="font-size: 16px; font-family: sans-serif; margin-left: 10px;"><a style="color: rgb(51, 51, 51); text-decoration: none;" onmouseover="this.style.textDecoration=\'underline\'" onmouseout="this.style.textDecoration=\'none\'" href="http://www.etsy.com/treasury/'.$obj_treasury->get_id().' ">\''.$obj_treasury->get_title().'\'</a> by <a style="color: rgb(51, 51, 51); text-decoration: none;" onmouseover="this.style.textDecoration=\'underline\'" onmouseout="this.style.textDecoration=\'none\'" href="http://www.etsy.com/shop/'.$obj_treasury->get_shop().'">'.$obj_treasury->get_shop().'</a></h2><h2 style="font-size: 16px; font-family: sans-serif; margin-left: 10px;">'.$obj_treasury->get_description().'<br></h2>';
	}

	/**
	 * Get Table header
	 *
	 * @param String $display_format Display format of treasury listing
	 * @return String
	 */
	public function get_table_header_html() {
		return '<table style="border-spacing: 8px; width: auto; border-collapse: separate; line-height: 19px;"><tbody>';

	}

	/**
	 * Get Table rows html
	 *
	 * @param Treasury $obj_treasury Treasury object
	 * @param Array $listings Array of listings
	 * @param String $no_of_items No of individual listings
	 * @param Int $no_of_items Columns to display
	 * @param String $display_format Display format
	 * @return String
	 */
	private function get_table_rows_html( $obj_treasury, $listings, $no_of_items, $display ) {
		$cnt = 0;
		$table_rows_html = '';
		$td_html = '';
			
		while ($cnt < $no_of_items) {

			if ( $cnt == 0 ) {
				$table_rows_html .= '<tr>';
				$start = 0;
			}
			else {
				$table_rows_html .= '</tr><tr>';
				$start = $start + $display->get_no_columns();
			}

			foreach ( new LimitIterator( $listings, $start, $display->get_no_columns()) as $obj_listing ) {
				$td_html = $this->get_table_row($display, $obj_listing);
				$td_html .= $this->get_links($display, $obj_listing);
				$td_html .='</td>';

				$table_rows_html .= $td_html;
				$cnt++;

				if ( $cnt >= $no_of_items )
				break;
			}
		}

		$table_rows_html .= '</tr>';

		return $table_rows_html;
	}
	/**
	 * Return table row html
	 * 
	 * @param $display Display object
	 * @param $obj_listing Listing object
	 */
	public function get_table_row($display, $obj_listing) {
		$td_html = '<td style="border: 1px solid rgb(236, 236, 236); padding: 6px; text-align: left; width:'.$display->width.'; height:'.$display->height.';"><a style="text-decoration: none;" href="http://www.etsy.com/listing/'.$obj_listing->get_listing_id().'" onmouseover="this.style.textDecoration=\'underline\'" onmouseout="this.style.textDecoration=\'none\'"> <img title="'.$obj_listing->get_title().' - '.$obj_listing->get_shop_name().'" alt="'.$obj_listing->get_title().' - '.$obj_listing->get_shop_name().'" style="border: medium none; padding: 0px;" src="'.$obj_listing->get_image_url().'" width="'.$display->width.'"/><br> </a>';

		return $td_html;
	}
    /**
     * Return links of Etsy treasury listing
     * 
     * @param $display Display object
     * @param $obj_listing Listing object
     */
    
	public function get_links($display, $obj_listing) {
		return '';
	}

}