<?php
class Complete_View extends View_Strategy {
	public function get_links($display, $obj_listing) {
		$td_html = '<a style="text-decoration: none;" title="'.$obj_listing->get_title().'" href="http://www.etsy.com/listing/'.$obj_listing->get_listing_id().'" onmouseover="this.style.textDecoration=\'underline\'" onmouseout="this.style.textDecoration=\'none\'"><span style="color: rgb(102, 102, 102); font-size: '.$display->font_size.'; font-family: sans-serif;">'.Util::truncate($obj_listing->get_title(), $display->listing_title_length).'</span></a><br><div style="font-size: '.$display->font_size.'; font-family: sans-serif; float: left; margin-top: 0px; margin-bottom: 0px;"><a style="text-decoration: none; color: rgb(178, 178, 178);" title="'.$obj_listing->get_shop_name().'" href="http://www.etsy.com/shop/'.$obj_listing->get_shop_name().'" onmouseover="this.style.textDecoration=\'underline\'" onmouseout="this.style.textDecoration=\'none\'">'.Util::truncate($obj_listing->get_shop_name(), $display->shop_title_length).'</a></div><div style="color: rgb(120, 192, 66); font-size: '.$display->font_size.'; font-family: sans-serif; float: right; margin-top: 0px; margin-bottom: 0px;">'.'$'.$obj_listing->get_price().'</div>';

		return $td_html;
	}
		
}