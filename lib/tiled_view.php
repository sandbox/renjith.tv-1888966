<?php
class Tiled_View extends View_Strategy {
	// Override
	public function get_table_header_html() {
		return '<table style="width:auto;border-collapse:collapse;border:0;"><tbody>';
	}
	//Override
	public function get_table_row($display, $obj_listing) {
		$td_html = '<td style="border:0 none; margin:0; padding:0;"><a style="border:0 none; float:left; width:'.$display->width.'; height:'.$display->tiled_height.';" href="http://www.etsy.com/listing/'.$obj_listing->get_listing_id().'"> <img alt="'.$obj_listing->get_title().' - '.$obj_listing->get_shop_name().'" title="'.$obj_listing->get_title().' - '.$obj_listing->get_shop_name().'" width="'.$display->width.'" height="'.$display->tiled_height.'" style="border:0 none;" src="'.$obj_listing->get_image_url().'"/> </a>';

		return $td_html;
	}
}