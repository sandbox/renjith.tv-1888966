<?php
class Tiny_Display extends Display {

	public function __construct(View_Strategy $view_strategy, Treasury $treasury, $column_count) {
		parent::__construct($view_strategy, $treasury, $column_count);
		$this->set_properties();
	}

	public function set_properties() {
		$this->width = '110px';
		$this->height = '110px';
		$this->tiled_height = '110px';
		$this->font_size = '8px';
		$this->listing_title_length = 22;
		$this->shop_title_length = 16;
	}
}