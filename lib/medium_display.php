<?php
class Medium_Display extends Display {

	public function __construct(View_Strategy $view_strategy, Treasury $treasury, $column_count) {
		parent::__construct($view_strategy, $treasury, $column_count);
		$this->set_properties();
	}

	public function set_properties() {
		$this->width = '140px';
		$this->height = '140px';
		$this->tiled_height = '140px';
		$this->font_size = '10px';
		$this->listing_title_length = 26;
		$this->shop_title_length = 16;
	}
}