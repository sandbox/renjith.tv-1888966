<?php
class Very_Large_Display extends Display {

	public function __construct(View_Strategy $view_strategy, Treasury $treasury, $column_count) {
		parent::__construct($view_strategy, $treasury, 1);
		$this->set_properties();
	}

	public function set_properties() {
		$this->width = '570px';
		$this->height = '570px';
		$this->tiled_height = '570px';
		$this->listing_title_length = 80;
		$this->shop_title_length = 60;
	}
}